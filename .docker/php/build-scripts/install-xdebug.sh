#!/usr/bin/env sh

set -eux;

apk add --no-cache --virtual .build-deps \
  $PHPIZE_DEPS \
  icu-dev \
  libzip-dev \
  linux-headers \
  zlib-dev;

pecl install xdebug-${XDEBUG_VERSION};
pecl clear-cache;

docker-php-ext-enable xdebug;

runDeps="$(
  scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
    | tr ',' '\n' \
    | sort -u \
    | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
)";

apk add --no-cache --virtual .api-phpexts-rundeps $runDeps;
apk del .build-deps;

echo "xdebug.client_host=host.docker.internal" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
		&& echo "xdebug.start_with_request=trigger" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
		&& echo "xdebug.mode=coverage" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini;