# Inspiration from…
# https://github.com/paslandau/docker-php-tutorial

PACKAGE?=

##@ [Application: Dev]

composer: ## Run composer
	@"$(MAKE)" docker-run CMD="composer $(CMD)"

composer-req: CMD=require $(PACKAGE) ## Require a composer package
composer-req: composer docker-build

composer-install: docker-build ## Install composer packages in all containers by rebuilding the Docker containers