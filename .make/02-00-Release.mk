# Inspiration from…
# https://github.com/paslandau/docker-php-tutorial

##@ [Release Automation]

RELEASE_TYPE?=
RELEASE_TYPES := major minor patch rc beta alpha
RELEASE_VERSION?=

CHANGELOG_CMD=php vendor/bin/conventional-changelog
CHANGELOG_ARGS:= --no-change-without-commits --merged

# Variable with command to retrieve the current package version from composer
COMPOSER_VERSION=$(EXECUTE_IN_CONTAINER) cat composer.json | jq -r '"v" + .version'

.PHONY: release%
release%: PHP_VERSION=82

.PHONY: verify-release-type
verify-release-type:
	@$(if $(RELEASE_TYPE),,$(error RELEASE_TYPE must be provided, and be one of $(RELEASE_TYPES)))
	@$(if $(filter $(RELEASE_TYPE),$(RELEASE_TYPES)),,$(error RELEASE_TYPE '$(RELEASE_TYPE)' is not one of $(RELEASE_TYPES)))

.PHONY: release-changelog-preflight
release-changelog-preflight: # Checks that everything is ready to generate the CHANGELOG
	git checkout main
	@status=$$(git status --porcelain composer.json CHANGELOG); \
		if [ ! -z "$${status}" ]; \
		then \
		  	echo ; \
		  	echo "*************************************************************"; \
			echo "* Error - working directory is dirty. Commit those changes! *"; \
		  	echo "*************************************************************"; \
		  	echo ; \
			exit 1; \
		fi

.PHONY: release-changelog-recreate
release-changelog-recreate: release-changelog-preflight ## Resets the CHANGELOG by recreating from the complete repo history
	$(EXECUTE_IN_CONTAINER) $(CHANGELOG_CMD) $(CHANGELOG_ARGS) --history

.PHONY: release-changelog
release-changelog: verify-release-type release-changelog-preflight ## Updates the CHANGELOG with a new release.  Requires RELEASE_TYPE of 'major', 'minor' or 'patch'.
	# Generate the CHANGELOG
	$(EXECUTE_IN_CONTAINER) $(CHANGELOG_CMD) $(CHANGELOG_ARGS) --$(RELEASE_TYPE)

.PHONY: release
release: release-changelog ## Updates the CHANGELOG with a new release.  Requires RELEASE_TYPE of 'major', 'minor' or 'patch'.
    # Now commit all the things
    # 1. Look up the new version in the updated composer.json file
    # 2. Commit the CHANGELOG
    # 3. Tag the release
    # 4. Push the release commit
    # 5. Push the tag
    # 6. Reset composer.json (since it's best practice not to include it, since
    #    packagist uses repo tags)
	@RELEASE_VERSION=$$($(COMPOSER_VERSION)) ; \
		if [ "v" == "$${RELEASE_VERSION}" ]; \
		then \
		  echo ; \
		  echo "***********************************************************" ; \
		  echo "* [ERROR] Release version not found in composer.json file * " ; \
		  echo "***********************************************************" ; \
		  echo ; \
		  exit 1 ; \
		fi; \
		git add CHANGELOG.md ; \
		git commit CHANGELOG.md -m "release: $${RELEASE_VERSION}" ; \
		git tag $${RELEASE_VERSION} ; \
		git push ; \
		git push origin "$${RELEASE_VERSION}" ; \
		git checkout composer.json

release-major: RELEASE_TYPE=major
release-major: release

release-minor: RELEASE_TYPE=minor
release-minor: release

release-patch: RELEASE_TYPE=patch
release-patch: release

release-rc: RELEASE_TYPE=rc
release-rc: release

release-beta: RELEASE_TYPE=beta
release-beta: release

release-alpha: RELEASE_TYPE=alpha
release-alpha: release