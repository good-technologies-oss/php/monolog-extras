# Inspiration from…
# https://github.com/paslandau/docker-php-tutorial

##@ [Application: QA]

# variables
CORES?=$(shell (nproc  || sysctl -n hw.ncpu) 2> /dev/null)

# constants
## files
ALL_FILES=./
APP_FILES=app/
TEST_FILES=tests/

## bash colors
RED:=\033[0;31m
GREEN:=\033[0;32m
YELLOW:=\033[0;33m
NO_COLOR:=\033[0m

# Tool CLI config
PHPSPEC_CMD=php vendor/bin/phpspec
PHPSPEC_ARGS= -c phpspec.yml
PHPSPEC_FILES=
INFECTION_CMD=php vendor/bin/infection
INFECTION_ARGS=
INFECTION_FILES=
PHPSTAN_CMD=php vendor/bin/phpstan analyse
PHPSTAN_ARGS=--level=9
PHPSTAN_FILES=$(APP_FILES) $(TEST_FILES)
PHPCS_CMD=php vendor/bin/phpcs
PHPCS_ARGS=--parallel=$(CORES) --standard=psr12
PHPCS_FILES=$(APP_FILES)
PHPCBF_CMD=php vendor/bin/phpcbf
PHPCBF_ARGS=$(PHPCS_ARGS)
PHPCBF_FILES=$(PHPCS_FILES)
PARALLEL_LINT_CMD=php vendor/bin/parallel-lint
PARALLEL_LINT_ARGS=-j 4 --exclude vendor/ --exclude .docker --exclude .git
PARALLEL_LINT_FILES=$(ALL_FILES)
COMPOSER_REQUIRE_CHECKER_CMD=php vendor/bin/composer-require-checker
COMPOSER_REQUIRE_CHECKER_ARGS=--ignore-parse-errors

# call with NO_PROGRESS=true to hide tool progress (makes sense when invoking multiple tools together)
NO_PROGRESS?=false
ifeq ($(NO_PROGRESS),true)
	PHPSTAN_ARGS+= --no-progress
	PARALLEL_LINT_ARGS+= --no-progress
else
	PHPCS_ARGS+= -p
	PHPCBF_ARGS+= -p
endif

# Use NO_PROGRESS=false when running individual tools.
# On  NO_PROGRESS=true  the corresponding tool has no output on success
#                       apart from its runtime but it will still print 
#                       any errors that occured. 
define execute
	if [ "$(NO_PROGRESS)" = "false" ]; then \
		eval "$(EXECUTE_IN_CONTAINER) $(1) $(2) $(3) $(4)"; \
	else \
		START=$$(date +%s); \
		printf "%-35s" "$@"; \
		if OUTPUT=$$(eval "$(EXECUTE_IN_CONTAINER) $(1) $(2) $(3) $(4)" 2>&1); then \
			printf " $(GREEN)%-6s$(NO_COLOR)" "done"; \
			END=$$(date +%s); \
			RUNTIME=$$((END-START)) ;\
			printf " took $(YELLOW)$${RUNTIME}s$(NO_COLOR)\n"; \
		else \
			printf " $(RED)%-6s$(NO_COLOR)" "fail"; \
			END=$$(date +%s); \
			RUNTIME=$$((END-START)) ;\
			printf " took $(YELLOW)$${RUNTIME}s$(NO_COLOR)\n"; \
			echo "$$OUTPUT"; \
			printf "\n"; \
			exit 1; \
		fi; \
	fi
endef

.PHONY: phpspec-desc
phpspec-desc: ## Create a spec file for the $CLASS e.g. `make phpspec-desc CLASS=Processor/PersonalDetailsProcessor`
	@$(call execute, $(DOCKER_COMPOSE) run --rm --user $(APP_USER_NAME) php73, $(PHPSPEC_CMD), $(PHPSPEC_ARGS), desc, "GoodTechnologies\\\\MonologExtras\\\\$(CLASS)")

.PHONY: phpspec-run-all
phpspec-run-all: ## Run tests on all PHP versions
	@"$(MAKE)" -j $(CORES) -k --no-print-directory --output-sync=target phpspec-run-all-exec NO_PROGRESS=true

.PHONY: phpspec-run-all-exec
phpspec-run-all-exec: \
	phpspec-run-73 \
	phpspec-run-74 \
	phpspec-run-80 \
	phpspec-run-81 \
	phpspec-run-82 \

.PHONY: phpspec-run
phpspec-run: phpspec-run-73 ## Alias for the phpspec-run-73

.PHONY: phpspec-run-73
phpspec-run-73: PHP_VERSION=73
phpspec-run-73: ## Run tests for PHP 7.3
	@$(call execute, $(PHPSPEC_CMD), $(PHPSPEC_ARGS), run, )

.PHONY: phpspec-run-74
phpspec-run-74: PHP_VERSION=74
phpspec-run-74: ## Run tests for PHP 7.4
	@$(call execute, $(PHPSPEC_CMD), $(PHPSPEC_ARGS), run, )

.PHONY: phpspec-run-80
phpspec-run-80: PHP_VERSION=80
phpspec-run-80: ## Run tests for PHP 8.0
	@$(call execute, $(PHPSPEC_CMD), $(PHPSPEC_ARGS), run, )

.PHONY: phpspec-run-81
phpspec-run-81: PHP_VERSION=81
phpspec-run-81: ## Run tests for PHP 8.1
	@$(call execute, $(PHPSPEC_CMD), $(PHPSPEC_ARGS), run, )

.PHONY: phpspec-run-82
phpspec-run-82: PHP_VERSION=82
phpspec-run-82: ## Run tests for PHP 8.2
	@$(call execute, $(PHPSPEC_CMD), $(PHPSPEC_ARGS), run, )

.PHONY: infection
infection: ## Run infection against all PHP versions
	@"$(MAKE)" -j $(CORES) -k --no-print-directory --output-sync=target infection-exec NO_PROGRESS=true

.PHONY: infection-exec
infection-exec: \
	infection-73 \
	infection-74 \
	infection-80 \
	infection-81 \
	infection-82

.PHONY: infection-73
infection-73: PHP_VERSION=73
infection-73: ## Run infection against PHP 7.3
	@$(call execute, $(INFECTION_CMD) $(INFECTION_ARGS) $(ARGS))

.PHONY: infection-74
infection-74: PHP_VERSION=74
infection-74: ## Run infection against PHP 7.4
	@$(call execute, $(INFECTION_CMD) $(INFECTION_ARGS) $(ARGS))

.PHONY: infection-80
infection-80: PHP_VERSION=80
infection-80: ## Run infection against PHP 8.0
	@$(call execute, $(INFECTION_CMD) $(INFECTION_ARGS) $(ARGS))

.PHONY: infection-81
infection-81: PHP_VERSION=81
infection-81: ## Run infection against PHP 8.1
	@$(call execute, $(INFECTION_CMD) $(INFECTION_ARGS) $(ARGS))

.PHONY: infection-82
infection-82: PHP_VERSION=82
infection-82: ## Run infection against PHP 8.2
	@$(call execute, $(INFECTION_CMD) $(INFECTION_ARGS) $(ARGS))

.PHONY: phplint
phplint: ## Run phplint on all files
	@$(call execute,$(PARALLEL_LINT_CMD),$(PARALLEL_LINT_ARGS),$(PARALLEL_LINT_FILES), $(ARGS))

.PHONY: phpcs
phpcs: ## Run style check on all application files
	@$(call execute, $(EXECUTE_IN_CONTAINER), $(PHPCS_CMD),$(PHPCS_ARGS),$(PHPCS_FILES), $(ARGS))

.PHONY: phpcbf
phpcbf: ## Run style fixer on all application files
	@$(call execute, $(EXECUTE_IN_CONTAINER), $(PHPCBF_CMD),$(PHPCBF_ARGS),$(PHPCBF_FILES), $(ARGS))

.PHONY: phpstan
phpstan:  ## Run static analyzer on all application and test files 
	@$(call execute, $(EXECUTE_IN_CONTAINER), $(PHPSTAN_CMD),$(PHPSTAN_ARGS),$(PHPSTAN_FILES), $(ARGS))

.PHONY: composer-require-checker
composer-require-checker: ## Run dependency checker
	@$(call execute, $(EXECUTE_IN_CONTAINER), $(COMPOSER_REQUIRE_CHECKER_CMD),$(COMPOSER_REQUIRE_CHECKER_ARGS),"", $(ARGS))

.PHONY: qa
qa: ## Run code quality tools on all files
	@"$(MAKE)" -j $(CORES) -k --no-print-directory --output-sync=target qa-exec NO_PROGRESS=true

.PHONY: qa-exec
qa-exec: phpstan \
	phplint \
	composer-require-checker \
	phpcs \
