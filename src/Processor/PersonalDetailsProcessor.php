<?php
declare(strict_types=1);

namespace GoodTechnologies\MonologExtras\Processor;

use GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\PersonalDetailsHandlerInterface;
use Monolog\Processor\ProcessorInterface;
use Psr\Log\LoggerInterface;

use function array_key_exists;
use function get_debug_type;
use function is_scalar;

class PersonalDetailsProcessor implements ProcessorInterface
{
    private const CONTEXT_IGNORE = self::class . '::ignore';


    /** @var iterable<PersonalDetailsHandlerInterface> */
    private $handlers;

    /** @var LoggerInterface */
    private $logger;


    /**
     * @param iterable<PersonalDetailsHandlerInterface> $handlers
     */
    public function __construct(
        iterable $handlers,
        LoggerInterface $logger
    ) {
        $this->handlers = $handlers;
        $this->logger = $logger;
    }


    public function __invoke(array $record): array
    {
        // Prevent infinite recursion caused by log messages logged inside this
        // processor.
        if ($record['context'][self::CONTEXT_IGNORE] ?? false) {
            return $record;
        }
        $toCleanse = ['message', 'context', 'extra'];

        foreach ($toCleanse as $key) {
            if (!array_key_exists($key, $record)) {
                continue;
            }

            $record[$key] = $this->cleanse($record[$key]);
        }

        return $record;
    }


    /**
     * @template T = object|array|string|bool|int|float|null
     *
     * @param T $data
     *
     * @return T
     */
    private function cleanse($data)
    {
        if (null === $data) {
            return null;
        }

        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $data[$key] = $this->cleanse($value);
            }

            return $data;
        }

        if (!is_scalar($data)) {
            $this->logger
                ->critical(
                    'Unable to remove personal details from log message as can '
                    . 'only process scalar values.  You might need to add a '
                    . 'processor upstream to convert objects of type {type} to '
                    . 'an scalar value, or an array of scalar values.',
                    [
                        'type' => get_debug_type($data),
                        // Prevent infinite recursion caused by log messages logged inside this
                        // processor.
                        self::CONTEXT_IGNORE => true,
                    ],
                );

            return $data;
        }

        foreach ($this->handlers as $handler) {
            if (!$handler->supports($data)) {
                continue;
            }

            $data = $handler->handle($data);
        }

        return $data;
    }
}
