<?php
declare(strict_types=1);

namespace GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor;

use Closure;

use function array_keys;
use function assert;
use function is_array;
use function is_string;
use function preg_match;
use function preg_match_all;
use function str_replace;

class RegexReplaceHandler implements PersonalDetailsHandlerInterface
{
    /** @var string */
    private $pattern;

    /** @var Closure */
    private $callback;


    /**
     * @param string $pattern
     * @param Closure $callback
     */
    public function __construct(
        string $pattern,
        Closure $callback
    ) {
        $this->callback = $callback;
        $this->pattern = $pattern;
    }


    /**
     * @param float|bool|int|string $value
     *
     * @return bool
     */
    final public function supports($value): bool
    {
        return is_string($value)
            && preg_match(
                $this->pattern,
                $value,
            );
    }


    /**
     * @template T = float|bool|int|string
     *
     * @param T $value
     *
     * @return T
     */
    final public function handle($value)
    {
        assert(is_string($value));
        assert($this->supports($value));

        preg_match_all(
            $this->pattern,
            $value,
            $matches,
        );

        assert([0] === array_keys($matches));
        $originalValues = $matches[0];
        assert(is_array($originalValues));

        $replacements = [];
        foreach ($originalValues as $originValue) {
            $replacements[] = ($this->callback)($originValue);
        }

        return str_replace($originalValues, $replacements, $value);
    }
}
