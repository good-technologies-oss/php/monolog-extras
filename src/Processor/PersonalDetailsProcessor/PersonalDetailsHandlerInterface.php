<?php
declare(strict_types=1);

namespace GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor;


interface PersonalDetailsHandlerInterface
{
    /**
     * Indicates whether this handler supports handling the given value
     *
     * @param string|bool|int|float $value
     */
    public function supports($value): bool;


    /**
     * @template T
     *
     * @param T $value
     *
     * @return T
     */
    public function handle($value);
}
