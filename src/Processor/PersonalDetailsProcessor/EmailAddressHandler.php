<?php
declare(strict_types=1);

namespace GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor;

use function assert;
use function explode;

class EmailAddressHandler extends RegexReplaceHandler
{
    private const PATTERN =
        '/[a-z0-9!#$%&\'*+\/=?^_‘{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_‘{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/';


    public function __construct()
    {
        parent::__construct(
            self::PATTERN,
            static function (string $originalValue): string {
                [$mailbox, $domain] = explode('@', $originalValue);

                return "{$mailbox[0]}*****@{$domain}";
            },
        );
    }
}
