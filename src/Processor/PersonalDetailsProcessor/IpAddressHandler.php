<?php
declare(strict_types=1);

namespace GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor;

use function ip2long;
use function long2ip;
use function rtrim;

final class IpAddressHandler extends RegexReplaceHandler
{
    private const PATTERN =
        '/(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/';


    public function __construct()
    {
        parent::__construct(
            self::PATTERN,
            function (string $originValue): string {
                return rtrim(
                        long2ip(
                            ip2long($originValue) & (~255),
                        ),
                        "0",
                    ) . "x";
            },
        );
    }
}
