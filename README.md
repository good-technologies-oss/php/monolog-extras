# Monolog Extras

A collection of handlers, processors, and other Monolog related classes.

# Usage instructions

```shell
composer require good-technologies/monolog-extras
```

The add any of the classes defined below to your Monolog set up.

## Processors

- [PersonalDetailsProcessor](./src/Processor/PersonalDetailsProcessor.php) for
  redacting personal details from log data using one or more
  [PersonalDetailsHandlerInterface](./src/Processor/PersonalDetailsProcessor/PersonalDetailsHandlerInterface.php)
  instances;

  Available `PersonalDetailsHandlerInterface` handlers are…
  - [EmailAddressHandler](./src/Processor/PersonalDetailsProcessor/EmailAddressHandler.php)
  - [IpAddressHandler](./src/Processor/PersonalDetailsProcessor/IpAddressHandler.php)

  Example…
    ```php
    <?php

    use GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor;use GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\EmailAddressHandler;use GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\IpAddressHandler;use Monolog\Handler\StreamHandler;use Monolog\Logger;


    $logger = new Logger();

    $handler = new StreamHandler('/logs/app.log')


    // Add to logger
    $logger->pushProcessor($personDetailsProcessor);


    // Add to specific handler
    $personDetailsProcessor = new PersonalDetailsProcessor([
            new EmailAddressHandler(),
            new IpAddressHandler(),
        ]);
    $handler->pushProcessor($personDetailsProcessor);
    $logger->pushHandler($handler);
    ```

# Setting up dev environment

```shell
# Load some useful aliases
. ./bin/aliases

# Build docker containers
dc build

# Install dependencies
composer install
```