<!--- BEGIN HEADER -->
# Changelog

All notable changes to this project will be documented in this file.
<!--- END HEADER -->

## [1.0.0](https://gitlab.com/good-technologies-oss/php/monolog-extras/compare/v0.0.0...v1.0.0) (2023-05-17)

### Features


##### Processors

* Add personal details stripping processor ([4cf5d2](https://gitlab.com/good-technologies-oss/php/monolog-extras/commit/4cf5d27639a8ca4e8ad280a5eb8af510178ec67f))
* Handler for email addresss filtering ([ff5bc0](https://gitlab.com/good-technologies-oss/php/monolog-extras/commit/ff5bc06e2598c7993a3b1d048c8fe88009982a61))
* IP Address handler ([21bf66](https://gitlab.com/good-technologies-oss/php/monolog-extras/commit/21bf66a241b79ced00c6fb7cca4a2b053b0716db))
* Regex personal details handler ([0d7a56](https://gitlab.com/good-technologies-oss/php/monolog-extras/commit/0d7a56e54a22bffa734ad47bf9847c549a2aca4e))

### Bug Fixes


##### Tests

* Fix assertion exceptions in PHP 8+ ([30b6e3](https://gitlab.com/good-technologies-oss/php/monolog-extras/commit/30b6e3121be89352d0e7b0c71c4d1835598acc84))


---

