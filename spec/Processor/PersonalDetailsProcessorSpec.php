<?php
declare(strict_types=1);

namespace spec\GoodTechnologies\MonologExtras\Processor;

use GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor;
use GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\PersonalDetailsHandlerInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;
use stdClass;

class PersonalDetailsProcessorSpec extends ObjectBehavior
{

    public function let(PersonalDetailsHandlerInterface $handler1, LoggerInterface $logger): void
    {
        $this->beConstructedWith([$handler1], $logger);
    }


    public function it_is_initializable(): void
    {
        $this->shouldHaveType(PersonalDetailsProcessor::class);
    }


    /**
     * @covers PersonalDetailsProcessor::__invoke
     */
    public function it_should_call_handlers_that_support_the_value(PersonalDetailsHandlerInterface $handler1): void
    {
        $record = [
            'message' => $message = 'A message',
        ];

        $handler1->supports($message)->willReturn(true);
        $handler1->handle($message)->willReturn("Processed {$message}");

        $this
            ->__invoke($record)
            ->shouldReturn([
                'message' => "Processed {$message}",
            ]);
    }


    /**
     * @covers PersonalDetailsProcessor::__invoke
     */
    public function it_should_not_call_handler_that_dont_support_the_value(
        PersonalDetailsHandlerInterface $handler1
    ):
    void {
        $record = [
            'message' => $message = 'A message',
        ];

        $handler1->supports($message)->willReturn(false);
        $handler1->handle($message)->shouldNotBeCalled();

        $this
            ->__invoke($record)
            ->shouldReturn([
                'message' => $message,
            ]);
    }


    /**
     * @covers PersonalDetailsProcessor::__invoke
     */
    public function it_should_support_multiple_handlers(
        PersonalDetailsHandlerInterface $handler1,
        PersonalDetailsHandlerInterface $handler2,
        LoggerInterface $logger
    ): void {
        $record = [
            'message' => $message = 'A message',
        ];

        $handler1->supports($message)->willReturn(true);
        $handler1->handle($message)->willReturn("Before {$message}");

        $handler2->supports("Before {$message}")->willReturn(true);
        $handler2->handle("Before {$message}")->willReturn("Before {$message} and after");

        $this->beConstructedWith([$handler1, $handler2], $logger);

        $this
            ->__invoke($record)
            ->shouldReturn(['message' => "Before {$message} and after"]);
    }


    /**
     * @covers PersonalDetailsProcessor::__invoke
     */
    public function it_should_process_context_items(PersonalDetailsHandlerInterface $handler1): void
    {
        $record = [
            'message' => 'A message',
            'context' => [
                'a' => [
                    'multi',
                    'depth' => [
                        'array',
                    ],
                ],
            ],
        ];

        $handler1->supports('multi')->willReturn(true);
        $handler1->supports('array')->willReturn(true);
        $handler1->supports(Argument::any())->willReturn(false);
        $handler1->handle('multi')->willReturn('multi-level');
        $handler1->handle('array')->willReturn('arrays');

        $this
            ->__invoke($record)
            ->shouldReturn([
                'message' => 'A message',
                'context' => [
                    'a' => [
                        'multi-level',
                        'depth' => [
                            'arrays',
                        ],
                    ],
                ],
            ]);
    }


    /**
     * @covers PersonalDetailsProcessor::__invoke
     */
    public function it_should_process_extra_items(PersonalDetailsHandlerInterface $handler1): void
    {
        $record = [
            'message' => 'A message',
            'extra' => [
                'a' => [
                    'multi',
                    'depth' => [
                        'array',
                    ],
                ],
            ],
        ];

        $handler1->supports('multi')->willReturn(true);
        $handler1->supports('array')->willReturn(true);
        $handler1->supports(Argument::any())->willReturn(false);
        $handler1->handle('multi')->willReturn('multi-level');
        $handler1->handle('array')->willReturn('arrays');

        $this
            ->__invoke($record)
            ->shouldReturn([
                'message' => 'A message',
                'extra' => [
                    'a' => [
                        'multi-level',
                        'depth' => [
                            'arrays',
                        ],
                    ],
                ],
            ]);
    }


    /**
     * @covers PersonalDetailsProcessor::__invoke
     */
    public function it_should_call_all_handlers_even_if_earlier_ones_dont_support(
        PersonalDetailsHandlerInterface $handler1,
        PersonalDetailsHandlerInterface $handler2,
        LoggerInterface $logger
    ): void {
        $record = [
            'message' => $message = 'A message',
        ];

        $handler1->supports($message)->willReturn(false);
        $handler1->handle($message)->willReturn("Before {$message}");

        $handler2->supports($message)->willReturn(true);
        $handler2->handle($message)->willReturn("{$message} and after");

        $this->beConstructedWith([$handler1, $handler2], $logger);

        $this
            ->__invoke($record)
            ->shouldReturn(['message' => "{$message} and after"]);
    }


    /**
     * @covers PersonalDetailsProcessor::__invoke
     */
    public function it_should_log_when_a_non_scalar_value_is_found(LoggerInterface $logger): void
    {
        $record = [
            'context' => [
                'anObject' => new stdClass(),
            ],
        ];

        $this
            ->__invoke($record)
            ->shouldReturn($record);

        $logger
            ->critical(
                'Unable to remove personal details from log message as can '
                . 'only process scalar values.  You might need to add a '
                . 'processor upstream to convert objects of type {type} to '
                . 'an scalar value, or an array of scalar values.',
                [
                    'type' => stdClass::class,
                    PersonalDetailsProcessor::class . '::ignore' => true,
                ],
            )
            ->shouldHaveBeenCalled();
    }


    /**
     * @covers \GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor::__invoke()
     */
    public function it_should_not_process_records_marked_for_ignoring(
        PersonalDetailsHandlerInterface $handler1
    ): void {
        $record = [
            'message' => 'A message',
            'context' => [
                PersonalDetailsProcessor::class . '::ignore' => true,
            ],
        ];

        $this($record)
            ->shouldReturn($record);

        $handler1
            ->supports(Argument::any())
            ->shouldNotHaveBeenCalled();
    }


    /**
     * @covers \GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor::__invoke()
     */
    public function it_should_ignore_null_values_in_context_or_extras(PersonalDetailsHandlerInterface $handler1): void
    {
        $this([
            'context' => [
                'nullValue' => null,
            ],
            'extra' => [
                'nullValue' => null,
            ],
        ]);

        $handler1
            ->supports(Argument::any())
            ->shouldNotHaveBeenCalled();
    }
}
