<?php
declare(strict_types=1);

namespace spec\GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor;

use GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\IpAddressHandler;
use PhpSpec\ObjectBehavior;

class IpAddressHandlerSpec extends ObjectBehavior
{
    public function it_is_initializable(): void
    {
        $this->shouldHaveType(IpAddressHandler::class);
    }


    /**
     * @covers \GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\IpAddressHandler::supports()
     */
    public function it_should_support_ip_addresses(): void
    {
        $this->supports('10.0.1.2')->shouldReturn(true);
    }


    /**
     * @covers \GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\IpAddressHandler::supports()
     */
    public function it_should_support_ip_addresses_within_strings(): void
    {
        $this
            ->supports('My IP address is 10.0.1.2 on my home network')
            ->shouldReturn(true);
    }


    /**
     * @covers \GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\IpAddressHandler::handle()
     */
    public function it_should_redact_an_ip_address(): void
    {
        $this
            ->handle('10.0.1.2')
            ->shouldReturn('10.0.1.x');
    }


    /**
     * @covers \GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\IpAddressHandler::handle()
     */
    public function it_should_redact_an_ip_address_within_a_string(): void
    {
        $this
            ->handle('My IP address is 10.0.1.2 on my home network')
            ->shouldReturn('My IP address is 10.0.1.x on my home network');
    }


    /**
     * @covers \GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\IpAddressHandler::handle()
     */
    public function it_should_redact_multiple_ip_addresses_within_a_string(): void
    {
        $this
            ->handle('My IP address is 10.0.1.1 on my home network, and 192.168.12.30 at work')
            ->shouldReturn('My IP address is 10.0.1.x on my home network, and 192.168.12.x at work');
    }
}
