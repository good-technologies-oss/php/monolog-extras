<?php

namespace spec\GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor;

use GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\EmailAddressHandler;
use PhpSpec\ObjectBehavior;

use function version_compare;

use const PHP_VERSION;

class EmailAddressHandlerSpec extends ObjectBehavior
{
    public function it_is_initializable(): void
    {
        $this->shouldHaveType(EmailAddressHandler::class);
    }


    /**
     * @covers \GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\EmailAddressHandler::supports()
     */
    public function it_should_support_email_addresses(): void
    {
        $this->supports('me@example.com')->shouldReturn(true);
    }


    /**
     * @covers \GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\EmailAddressHandler::supports()
     */
    public function it_should_support_email_addresses_within_strings(): void
    {
        $this
            ->supports('My email address is me@example.com, for work emails')
            ->shouldReturn(true);
    }


    /**
     * @covers \GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\EmailAddressHandler::handle()
     */
    public function it_should_redact_an_email_address(): void
    {
        $this
            ->handle('me@example.com')
            ->shouldReturn('m*****@example.com');
    }


    /**
     * @covers \GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\EmailAddressHandler::handle()
     */
    public function it_should_redact_an_email_address_within_a_string(): void
    {
        $this
            ->handle('My email address is me@example.com, for work emails')
            ->shouldReturn('My email address is m*****@example.com, for work emails');
    }


    /**
     * @covers \GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\EmailAddressHandler::handle()
     */
    public function it_should_redact_multiple_email_addresses_within_a_string(): void
    {
        $this
            ->handle('My email address is me@work.com for work emails and us@home.com for personal ones')
            ->shouldReturn('My email address is m*****@work.com for work emails and u*****@home.com for personal ones');
    }


    /**
     * @covers \GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\EmailAddressHandler::handle()
     */
    public function it_should_ensure_valid_email_address_when_handling(): void
    {
        $exceptionClass = '\AssertionError';
        if (version_compare(PHP_VERSION, '8.0.0', '<')) {
            $exceptionClass = '\PhpSpec\Exception\Example\ErrorException';
        }

        $this
            ->shouldThrow($exceptionClass)
            ->duringHandle('not@an@email@address');
    }
}
