<?php
declare(strict_types=1);

namespace spec\GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor;

use GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\RegexReplaceHandler;
use PhpSpec\ObjectBehavior;

use function version_compare;

use const PHP_VERSION;

class RegexReplaceHandlerSpec extends ObjectBehavior
{
    public function let(): void
    {
        $this->beConstructedWith('/^wibble/', function ($value) {
            return "[{$value}]";
        });
    }


    function it_is_initializable()
    {
        $this->shouldHaveType(RegexReplaceHandler::class);
    }


    /**
     * @covers \GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\RegexReplaceHandler::supports()
     */
    public function it_should_support_values_that_match(): void
    {
        $this
            ->supports('wibble.wobble')
            ->shouldReturn(true);
    }


    /**
     * @covers \GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\RegexReplaceHandler::supports()
     */
    public function it_should_not_support_value_that_dont_match(): void
    {
        $this
            ->supports('wobble.wibble')
            ->shouldReturn(false);
    }


    /**
     * @covers \GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\RegexReplaceHandler::handle()
     */
    public function it_should_handle_values_using_callback(): void
    {
        $this
            ->handle('wibble.wobble')
            ->shouldReturn('[wibble].wobble');
    }


    /**
     * @covers \GoodTechnologies\MonologExtras\Processor\PersonalDetailsProcessor\RegexReplaceHandler::handle()
     */
    public function it_should_throw_if_handling_an_unsupported_value(): void
    {
        $exceptionClass = '\AssertionError';
        if (version_compare(PHP_VERSION, '8.0.0', '<')) {
            $exceptionClass = '\PhpSpec\Exception\Example\ErrorException';
        }

        $this
            ->shouldThrow($exceptionClass)
            ->duringHandle('wobble.wibble');
    }
}
